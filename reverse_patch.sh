#! /bin/sh
if grep -q "$1" p.log; then
  sudo patch -R -p1 -d/ < "patch/$1" && sed -i "\:$1:d" p.log && echo "Success. Patch $1 reversed."
else
  echo "Failure. Patch $1 is not currently applied, so cannot be reversed."
fi
