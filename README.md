Patch system files
====
Some very simple scaffolding to tweak operating system files
and track which tweaks have been applied.
The tweaks themselves are not provided in this repo.

Requirement
----
`POSIX.1` or later.
Almost any shell and almost any version of `patch(1)` should work
but try non-POSIX at your own risk.

How to use
----
Suppose we want to tweak `/etc/foo.txt`.

Set the working directory to `syspatch`.

Copy `/etc/foo.txt` to `syspatch/altroot/etc/foo.txt`
and make the desired changes to the latter of these.

Set `xp=etc/foo.txt`
There must be no slash immediately after the equals sign.
There must be no slash at the end of the value of xp, even if it is a directory.

Generate the patch file. This will be
stored in `syspatch/patch/etc`. (Ensure that directory exists first.)
````
./generate_patch.sh $xp
````
At this point, `syspatch/altroot/etc/foo.txt`
is redundant and can be deleted.

Apply the patch to the system file.
This command also records in the log file that the patch has been applied.
````
sudo ./apply_patch.sh $xp
````
We can apply patches that have been generated in other ways,
but the generate script is the simplest case.

Suppose the patch breaks something and we want to go back to how things were.
Unless the system file has changed radically since the patch, this command will
undo the patch and remove the patch's entry from the log file.
````
sudo ./reverse_patch.sh $xp
````

Note that `syspatch/patch` does not have to be a directory.
It can be a symlink to patch files we have stored elsewhere,
perhaps in another git repository.
This will look something like the following.
````
ln -s ~/src/patchrepo/patch ~/src/syspatch/patch
````

Motivation
----
We often want to make changes to an operating system's files.
When doing so, we face some constraints. System files usually
 1. live in or near the root of the filesystem;
 2. come with read/write restrictions on nearby files and directories;
 3. affect how the system behaves for all users;
 4. can make the system misbehave, if set incorrectly.

`patch(1)` does not alter anything but its target files,
so it gives us the best chance to satisfy constraint 1 and 2.
It can usually reverse its changes exactly in a single command so,
if we break our system (i.e. violate constraint 3 or 4),
we have a good chance of returning to a working state.

The second-best solution would be `ed` or `sed` scripts.
That would require us to write a patch and a reverse script by hand for each
file we wanted to edit.

The third-best solution would be a repo of our desired configuration
files in their entirety, which we would `cp` into place.
That would lose us the ability to apply patches in arbitrary combinations
(unless we created and stored a combinatorial explosion of files).

We are duplicating a subset of the features of `quilt`,
but, for the subset of features we need, our way is simpler.
